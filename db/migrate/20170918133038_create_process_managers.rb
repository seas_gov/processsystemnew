class CreateProcessManagers < ActiveRecord::Migration[5.1]
  def change
    create_table :process_managers do |t|
      t.string :process_number
      t.timestamp :opening_hour_date
      t.string :subject
      t.text :notes
      t.string :key_words, array: true, default: []
      t.integer :confidentiality_level
      t.integer :priority
      t.integer :created_by
      t.integer :status

      t.timestamps
    end
  end
end
