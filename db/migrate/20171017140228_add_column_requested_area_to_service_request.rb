class AddColumnRequestedAreaToServiceRequest < ActiveRecord::Migration[5.1]
  def change
    add_column :service_requests, :requested_area, :integer
  end
end
