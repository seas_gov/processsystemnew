class RenameColumnServiceRequestingToServiceRequest < ActiveRecord::Migration[5.1]
  def change
  	rename_column :service_requests, :service_requested, :service_id
  end
end
