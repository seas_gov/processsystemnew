class AddColumnAreaIdToService < ActiveRecord::Migration[5.1]
  def change
    add_reference :services, :area, foreign_key: true
  end
end
