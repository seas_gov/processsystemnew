class AddColumnProcessAuthorsToProcessManager < ActiveRecord::Migration[5.1]
  def change
    add_column :process_managers, :authors, :string, array: true, default: []
  end
end
