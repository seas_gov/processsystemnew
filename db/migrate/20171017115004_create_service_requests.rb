class CreateServiceRequests < ActiveRecord::Migration[5.1]
  def change
    create_table :service_requests do |t|
      t.date :date_request
      t.time :time_request
      t.text :description_problem
      t.integer :service_requested
      t.time :maximum_time_of_attendance
      t.references :unit, foreign_key: true
      t.string :area_requesting
      t.integer :user_requesting
      t.integer :user
      t.date :date_response
      t.time :time_response
      t.integer :user_responsible_attendance
      t.date :date_scheduled
      t.time :time_scheduled
      t.date :date_service
      t.time :time_service
      t.time :time_finalization_attendance
      t.text :notes
      t.references :status, foreign_key: true

      t.timestamps
    end
  end
end
