class CreateUnits < ActiveRecord::Migration[5.1]
  def change
    create_table :units do |t|
      t.string :name
      t.string :address
      t.integer :address_number
      t.string :add_address
      t.boolean :active

      t.timestamps
    end
  end
end
