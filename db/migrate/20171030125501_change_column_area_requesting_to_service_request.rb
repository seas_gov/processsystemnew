class ChangeColumnAreaRequestingToServiceRequest < ActiveRecord::Migration[5.1]
  def change
    change_column :service_requests, :area_requesting, :integer, using: 'area_requesting::integer'
  end
end
