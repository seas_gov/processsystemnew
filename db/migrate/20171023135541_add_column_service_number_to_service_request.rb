class AddColumnServiceNumberToServiceRequest < ActiveRecord::Migration[5.1]
  def change
    add_column :service_requests, :service_number, :integer
  end
end
