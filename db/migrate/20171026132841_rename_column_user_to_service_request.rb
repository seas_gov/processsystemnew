class RenameColumnUserToServiceRequest < ActiveRecord::Migration[5.1]
  def change
  	rename_column :service_requests, :user, :service_for
  end
end
