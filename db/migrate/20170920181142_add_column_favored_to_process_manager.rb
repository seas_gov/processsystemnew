class AddColumnFavoredToProcessManager < ActiveRecord::Migration[5.1]
  def change
    add_column :process_managers, :favored, :string, array: true, default: []
  end
end
