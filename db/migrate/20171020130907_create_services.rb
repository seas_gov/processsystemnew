class CreateServices < ActiveRecord::Migration[5.1]
  def change
    create_table :services do |t|
      t.string :name
      t.integer :sla_service_hour
      t.integer :sla_service_minute
      t.integer :function_id
      t.boolean :active

      t.timestamps
    end
  end
end
