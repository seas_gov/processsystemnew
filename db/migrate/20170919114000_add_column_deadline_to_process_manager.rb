class AddColumnDeadlineToProcessManager < ActiveRecord::Migration[5.1]
  def change
    add_column :process_managers, :deadline, :timestamp
  end
end
