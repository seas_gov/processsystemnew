class AddColumnFilesDirectoryToServiceRequest < ActiveRecord::Migration[5.1]
  def change
    add_column :service_requests, :files_directory, :string
  end
end
