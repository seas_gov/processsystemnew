class AddColumnSubareaRequestingToServiceRequest < ActiveRecord::Migration[5.1]
  def change
    add_column :service_requests, :subarea_requesting, :integer
  end
end
