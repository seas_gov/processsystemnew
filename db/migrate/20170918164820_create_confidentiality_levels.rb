class CreateConfidentialityLevels < ActiveRecord::Migration[5.1]
  def change
    create_table :confidentiality_levels do |t|
      t.string :description

      t.timestamps
    end
  end
end
