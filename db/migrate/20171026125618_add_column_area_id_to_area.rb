class AddColumnAreaIdToArea < ActiveRecord::Migration[5.1]
  def change
    add_column :areas, :area_id, :integer
  end
end
