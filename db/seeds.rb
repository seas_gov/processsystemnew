# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

ViprocProfile.create([{description: 'Tramitador'}, {description: 'Protocolador'}])
Priority.create([{description: 'Baixa'}, {description: 'Média'}, {description: 'Alta'}])
ConfidentialityLevel.create([{description: 'Privado'}, {description: 'Público'}])
Status.create([{description: 'Aguardando providências'}, {description: 'Em andamento'}, {description: 'Cancelado'}, {description: 'Finalizado'}])