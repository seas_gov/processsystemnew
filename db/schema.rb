# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171030125501) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "areas", force: :cascade do |t|
    t.string "name"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "unit_id"
    t.integer "area_id"
    t.index ["unit_id"], name: "index_areas_on_unit_id"
  end

  create_table "confidentiality_levels", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "priorities", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "process_managers", force: :cascade do |t|
    t.string "process_number"
    t.datetime "opening_hour_date"
    t.string "subject"
    t.text "notes"
    t.string "key_words", default: [], array: true
    t.integer "confidentiality_level"
    t.integer "priority"
    t.integer "created_by"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deadline"
    t.string "authors", default: [], array: true
    t.string "favored", default: [], array: true
  end

  create_table "service_requests", force: :cascade do |t|
    t.date "date_request"
    t.time "time_request"
    t.text "description_problem"
    t.integer "service_id"
    t.time "maximum_time_of_attendance"
    t.bigint "unit_id"
    t.integer "area_requesting"
    t.integer "user_requesting"
    t.integer "service_for"
    t.date "date_response"
    t.time "time_response"
    t.integer "user_responsible_attendance"
    t.date "date_scheduled"
    t.time "time_scheduled"
    t.date "date_service"
    t.time "time_service"
    t.time "time_finalization_attendance"
    t.text "notes"
    t.bigint "status_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "requested_area"
    t.integer "subarea_requesting"
    t.integer "service_number"
    t.string "files_directory"
    t.index ["status_id"], name: "index_service_requests_on_status_id"
    t.index ["unit_id"], name: "index_service_requests_on_unit_id"
  end

  create_table "services", force: :cascade do |t|
    t.string "name"
    t.integer "sla_service_hour"
    t.integer "sla_service_minute"
    t.integer "function_id"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "area_id"
    t.index ["area_id"], name: "index_services_on_area_id"
  end

  create_table "statuses", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "units", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.integer "address_number"
    t.string "add_address"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_areas", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "area_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["area_id"], name: "index_user_areas_on_area_id"
    t.index ["user_id"], name: "index_user_areas_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "viproc_profiles", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "areas", "units"
  add_foreign_key "service_requests", "statuses"
  add_foreign_key "service_requests", "units"
  add_foreign_key "services", "areas"
  add_foreign_key "user_areas", "areas"
  add_foreign_key "user_areas", "users"
end
