require 'test_helper'

class UserAreasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user_area = user_areas(:one)
  end

  test "should get index" do
    get user_areas_url
    assert_response :success
  end

  test "should get new" do
    get new_user_area_url
    assert_response :success
  end

  test "should create user_area" do
    assert_difference('UserArea.count') do
      post user_areas_url, params: { user_area: { area_id: @user_area.area_id, user_id: @user_area.user_id } }
    end

    assert_redirected_to user_area_url(UserArea.last)
  end

  test "should show user_area" do
    get user_area_url(@user_area)
    assert_response :success
  end

  test "should get edit" do
    get edit_user_area_url(@user_area)
    assert_response :success
  end

  test "should update user_area" do
    patch user_area_url(@user_area), params: { user_area: { area_id: @user_area.area_id, user_id: @user_area.user_id } }
    assert_redirected_to user_area_url(@user_area)
  end

  test "should destroy user_area" do
    assert_difference('UserArea.count', -1) do
      delete user_area_url(@user_area)
    end

    assert_redirected_to user_areas_url
  end
end
