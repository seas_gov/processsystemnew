require 'test_helper'

class ServiceRequestsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @service_request = service_requests(:one)
  end

  test "should get index" do
    get service_requests_url
    assert_response :success
  end

  test "should get new" do
    get new_service_request_url
    assert_response :success
  end

  test "should create service_request" do
    assert_difference('ServiceRequest.count') do
      post service_requests_url, params: { service_request: { area_requesting: @service_request.area_requesting, date_request: @service_request.date_request, date_response: @service_request.date_response, date_scheduled: @service_request.date_scheduled, date_service: @service_request.date_service, description_problem: @service_request.description_problem, maximum_time_of_attendance: @service_request.maximum_time_of_attendance, notes: @service_request.notes, service_requested: @service_request.service_requested, status_id: @service_request.status_id, time_finalization_attendance: @service_request.time_finalization_attendance, time_request: @service_request.time_request, time_response: @service_request.time_response, time_scheduled: @service_request.time_scheduled, time_service: @service_request.time_service, unit_id: @service_request.unit_id, user: @service_request.user, user_requesting: @service_request.user_requesting, user_responsible_attendance: @service_request.user_responsible_attendance } }
    end

    assert_redirected_to service_request_url(ServiceRequest.last)
  end

  test "should show service_request" do
    get service_request_url(@service_request)
    assert_response :success
  end

  test "should get edit" do
    get edit_service_request_url(@service_request)
    assert_response :success
  end

  test "should update service_request" do
    patch service_request_url(@service_request), params: { service_request: { area_requesting: @service_request.area_requesting, date_request: @service_request.date_request, date_response: @service_request.date_response, date_scheduled: @service_request.date_scheduled, date_service: @service_request.date_service, description_problem: @service_request.description_problem, maximum_time_of_attendance: @service_request.maximum_time_of_attendance, notes: @service_request.notes, service_requested: @service_request.service_requested, status_id: @service_request.status_id, time_finalization_attendance: @service_request.time_finalization_attendance, time_request: @service_request.time_request, time_response: @service_request.time_response, time_scheduled: @service_request.time_scheduled, time_service: @service_request.time_service, unit_id: @service_request.unit_id, user: @service_request.user, user_requesting: @service_request.user_requesting, user_responsible_attendance: @service_request.user_responsible_attendance } }
    assert_redirected_to service_request_url(@service_request)
  end

  test "should destroy service_request" do
    assert_difference('ServiceRequest.count', -1) do
      delete service_request_url(@service_request)
    end

    assert_redirected_to service_requests_url
  end
end
