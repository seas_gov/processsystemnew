require 'test_helper'

class ProcessManagersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @process_manager = process_managers(:one)
  end

  test "should get index" do
    get process_managers_url
    assert_response :success
  end

  test "should get new" do
    get new_process_manager_url
    assert_response :success
  end

  test "should create process_manager" do
    assert_difference('ProcessManager.count') do
      post process_managers_url, params: { process_manager: { confidentiality_level: @process_manager.confidentiality_level, created_by: @process_manager.created_by, key_words: @process_manager.key_words, notes: @process_manager.notes, opening_hour_date: @process_manager.opening_hour_date, priority: @process_manager.priority, process_number: @process_manager.process_number, status: @process_manager.status, subject: @process_manager.subject } }
    end

    assert_redirected_to process_manager_url(ProcessManager.last)
  end

  test "should show process_manager" do
    get process_manager_url(@process_manager)
    assert_response :success
  end

  test "should get edit" do
    get edit_process_manager_url(@process_manager)
    assert_response :success
  end

  test "should update process_manager" do
    patch process_manager_url(@process_manager), params: { process_manager: { confidentiality_level: @process_manager.confidentiality_level, created_by: @process_manager.created_by, key_words: @process_manager.key_words, notes: @process_manager.notes, opening_hour_date: @process_manager.opening_hour_date, priority: @process_manager.priority, process_number: @process_manager.process_number, status: @process_manager.status, subject: @process_manager.subject } }
    assert_redirected_to process_manager_url(@process_manager)
  end

  test "should destroy process_manager" do
    assert_difference('ProcessManager.count', -1) do
      delete process_manager_url(@process_manager)
    end

    assert_redirected_to process_managers_url
  end
end
