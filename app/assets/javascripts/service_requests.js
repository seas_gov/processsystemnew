var ready = function() {
	$('#service_request_user').hide();

	$('#unit').change(function() {
		var unit_id = $('#unit').val();
		$.ajax({
			url: '/service_requests/areas',
			method: 'post',
			data: {unit_id: unit_id},
			success: function() {
				if ($('#area option:selected').val() != '') {
					var area_id = $('#area option:selected').val()
					$.ajax({
						url: '/service_requests/services_area',
						method: 'POST',
						data: {area_id: area_id}
					});
				}
			}
		});
	});

	$('#area').change(function() {
		var area_id = $('#area').val();
		$.ajax({
			url: '/service_requests/services_area',
			method: 'POST',
			data: {area_id: area_id}
		});
	});

	$('#new-user').click(function() {
		if(this.checked){
			$('#service_request_user').show();
			$('#service_request_user > select').select2({
		    theme: 'bootstrap'
		  });
    } else { 
			$('#service_request_user').hide();
    }
	})
};

if (typeof Turbolinks == "undefined") {
  $(document).ready(ready);
} else {
  $(document).on("turbolinks:load", ready);
}