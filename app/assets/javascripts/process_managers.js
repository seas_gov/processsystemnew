var ready = function() {
	function clear_form() {
		$('#form').trigger('reset');
	}

	$('#process_manager_process_number').keyup(function() {
		if (this.value.length == 12) {
			var process_number = this.value
			$.ajax({
				method: 'POST',
				url: '/process_managers/search_process',
				data: { process_number: process_number},
				success: function(retorno) {
					// $('#form').html(retorno);
					ready()
				}
			});
		}
	});
}
	
if (typeof Turbolinks == "undefined") {
  $(document).ready(ready);
} else {
  $(document).on("turbolinks:load", ready);
}