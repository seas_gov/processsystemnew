// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-mask
//= require bootstrap-sprockets
//= require bootstrap-datepicker
//= require select2
//= require turbolinks
//= require_tree .

var ready = function() {
  $('.datepicker').datepicker({
    language: 'pt-BR',
    todayHighlight: true,
    autoclose: true
  });

  $('.select2').select2({
    theme: 'bootstrap'
  });

  $('.select2-tag').select2({
    theme: 'bootstrap',
    tags: true,
    placeholder: 'Selecione',
    language: {
      noResults: function () {
        return "";
      }
    }
  });

  $('.alert').fadeTo(700, 0).slideUp(700, function() {
    $(this).remove(); 
  });

  $('.mask_process').mask('9999999/9999');

  $('.mask_date').mask('99/99/9999');

  $('.mask_hour').mask('99:99');
}

if (typeof Turbolinks == "undefined") {
  $(document).ready(ready);
} else {
  $(document).on("turbolinks:load", ready);
}