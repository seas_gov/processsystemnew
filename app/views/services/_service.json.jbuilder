json.extract! service, :id, :name, :sla_service_hour, :sla_service_minute, :function_id, :active, :created_at, :updated_at
json.url service_url(service, format: :json)
