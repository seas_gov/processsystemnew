json.extract! unit, :id, :name, :address, :address_number, :add_address, :active, :created_at, :updated_at
json.url unit_url(unit, format: :json)
