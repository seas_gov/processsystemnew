json.extract! process_manager, :id, :process_number, :opening_hour_date, :subject, :notes, :key_words, :confidentiality_level, :priority, :created_by, :status, :created_at, :updated_at
json.url process_manager_url(process_manager, format: :json)
