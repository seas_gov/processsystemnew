json.extract! user_area, :id, :user_id, :area_id, :created_at, :updated_at
json.url user_area_url(user_area, format: :json)
