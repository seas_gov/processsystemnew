class ViprocInformation < ApplicationRecord
	require 'savon'

	HOMOLOGACAO = 'http://viprocwsejb-h.seplag.ce.gov.br/viprocws/ViprocService?wsdl'
	# PRODUCAO = 'http://viprocwsejb.seplag.ce.gov.br/viprocws/ViprocService?wsdl'

	CLIENT = Savon.client(element_form_default: :unspecified, wsdl: HOMOLOGACAO)

	def self.criar_processo(parametros)
		CLIENT.call(:criar_processo, message: parametros)
	end

	def self.listar_assuntos
		CLIENT.call(:listar_assuntos).body[:listar_assuntos_response][:return].sort
	end

	def self.listar_sigla_orgaos
		CLIENT.call(:listar_sigla_orgaos).body[:listar_sigla_orgaos_response][:return].sort
	end

	def self.listar_sigla_lotacoes
		CLIENT.call(:listar_sigla_lotacoes)
	end

end
