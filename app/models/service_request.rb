class ServiceRequest < ApplicationRecord
  belongs_to :unit, required: false
  belongs_to :status, required: false
  belongs_to :service
end
