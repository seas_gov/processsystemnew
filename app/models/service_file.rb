class ServiceFile < ApplicationRecord

	def self.upload_file(files, directory)
		FileUtils.mkdir(directory) unless File.exists?(directory)
		if !files.nil?
			files.each do |file|
				File.open(File.join(directory, file.original_filename), 'wb') { |f| f.write(file.read) }
			end
		end
	end

	def self.download_file(directory)
		send_file directory
	end

	def self.list_service_files(service_id)
		directory = ServiceRequest.find(service_id).files_directory
		if !directory.nil?
			files = Dir.foreach(directory).map { |f| File.basename("#{f}") }.tap { |x| x.delete(".."); x.delete("."); }
		end
	end

end