class Area < ApplicationRecord
	has_many :user_areas
	belongs_to :unit
end