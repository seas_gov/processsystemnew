class ProcessManager < ApplicationRecord
	validates :process_number, :opening_hour_date, :subject, :notes, :confidentiality_level, :priority, :authors, presence: true
end