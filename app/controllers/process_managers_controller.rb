class ProcessManagersController < ApplicationController
  before_action :set_process_manager, only: [:show, :edit, :update, :destroy]

  # GET /process_managers
  # GET /process_managers.json
  def index
    @process_managers = ProcessManager.all
  end

  # GET /process_managers/1
  # GET /process_managers/1.json
  def show
  end

  # GET /process_managers/new
  def new
    @process_manager = ProcessManager.new
  end

  # GET /process_managers/1/edit
  def edit
  end

  # POST /process_managers
  # POST /process_managers.json
  def create
    @process_manager = ProcessManager.new(process_manager_params)
    @process_manager.key_words = params[:key_words]
    @process_manager.authors = params[:author]
    @process_manager.favored = params[:favored]

    # respond_to do |format|
      if @process_manager.save
        flash[:notice] = 'Processo incluido com sucesso.'
        redirect_to controller: :home, action: :index
        # format.html { redirect_to @process_manager, notice: 'Process manager was successfully created.' }
        # format.json { render :show, status: :created, location: @process_manager }
      else
        format.html { render :new }
        format.json { render json: @process_manager.errors, status: :unprocessable_entity }
      end
    # end
  end

  # PATCH/PUT /process_managers/1
  # PATCH/PUT /process_managers/1.json
  def update
    # respond_to do |format|
      if @process_manager.update(process_manager_params)
        @process_manager.key_words = params[:key_words]
        @process_manager.authors = params[:author]
        @process_manager.favored = params[:favored]
        @process_manager.save

        redirect_to action: :new
        # format.html { redirect_to @process_manager, notice: 'Process manager was successfully updated.' }
        # format.json { render :show, status: :ok, location: @process_manager }
      else
        format.html { render :edit }
        format.json { render json: @process_manager.errors, status: :unprocessable_entity }
      end
    # end
  end

  # DELETE /process_managers/1
  # DELETE /process_managers/1.json
  def destroy
    @process_manager.destroy
    respond_to do |format|
      format.html { redirect_to process_managers_url, notice: 'Process manager was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def search_process
    @process_manager = ProcessManager.find_by_process_number(params[:process_number])

    redirect_to '/process_managers/'+@process_manager.id.to_s+'/edit'
    # render partial: 'form', locals: { process_manager: @process_manager }
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_process_manager
      @process_manager = ProcessManager.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def process_manager_params
      params.require(:process_manager).permit(:process_number, :opening_hour_date, :subject, :notes, :key_words, :confidentiality_level, :priority, :created_by, :status, :deadline, :authors, :favored)
    end
end
