class ServiceRequestsController < ApplicationController
  before_action :set_service_request, only: [:show, :edit, :update, :destroy]

  # GET /service_requests
  # GET /service_requests.json
  def index
    @service_requests_area = ServiceRequest.all.order('date_request DESC, time_request DESC').where("requested_area = 1 and user_responsible_attendance is null").page(params[:page])
    @service_requests_attend = ServiceRequest.all.order('date_request DESC, time_request DESC').where("date_service is null and user_responsible_attendance = #{current_user.id}").page(params[:page])
    @service_requests_attended = ServiceRequest.all.order('date_request DESC, time_request DESC').where("date_service is not null and user_responsible_attendance = #{current_user.id}").page(params[:page]).per(18)
    @service_requests_requested = ServiceRequest.all.order('date_request DESC, time_request DESC').where("user_requesting = #{current_user.id}").page(params[:page]).per(18)
  end

  # GET /service_requests/1
  # GET /service_requests/1.json
  def show
    @list_service_files = ServiceFile.list_service_files(params[:id])
  end

  # GET /service_requests/new
  def new
    @area_requesting = UserArea.find_by(user_id: current_user.id)&.area
    @service_request = ServiceRequest.new
  end

  # GET /service_requests/1/edit
  def edit
  end

  # POST /service_requests
  # POST /service_requests.json
  def create
    @service_request = ServiceRequest.new(service_request_params)

    respond_to do |format|
      if @service_request.save
        @service_request.update_attributes(files_directory: "#{Rails.root}/service_files/#{@service_request.id}")
        directory = @service_request.files_directory
        files = params[:files_directory]
        ServiceFile.upload_file(files, directory)
        format.html { redirect_to @service_request, notice: 'Service request was successfully created.' }
        format.json { render :show, status: :created, location: @service_request }
      else
        format.html { render :new }
        format.json { render json: @service_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /service_requests/1
  # PATCH/PUT /service_requests/1.json
  def update
    respond_to do |format|
      if @service_request.update(service_request_params)
        format.html { redirect_to @service_request, notice: 'Service request was successfully updated.' }
        format.json { render :show, status: :ok, location: @service_request }
      else
        format.html { render :edit }
        format.json { render json: @service_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /service_requests/1
  # DELETE /service_requests/1.json
  def destroy
    @service_request.destroy
    respond_to do |format|
      format.html { redirect_to service_requests_url, notice: 'Service request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def areas
    @areas = Area.where("unit_id = #{params[:unit_id]} and area_id is null")
  end

  def services_area
    @services_area = Service.where(area_id: params[:area_id])
  end

  def schedule_request
    @service_request = ServiceRequest.find(params[:service_request_id])
  end

  def save_schedule_request
    @service_request = ServiceRequest.find(params[:service_request_id])
    @service_request.update_attributes(date_scheduled: params[:date_scheduled], time_scheduled: params[:time_scheduled])
    @service_request.save

    redirect_to action: :index
  end

  def answer_request
    @service_request_id = params[:service_request_id]
  end

  def save_answer_service
    @service_request = ServiceRequest.find(params[:service_request_id])
    @service_request.update_attributes(user_responsible_attendance: params[:user_responsible_attendance], date_service: params[:date_service], time_service: params[:time_service], status_id: params[:status], notes: params[:notes])
    @service_request.save

    redirect_to action: :index
  end

  def forward_responsible
    @service_request_id = params[:service_request_id]
  end

  def save_forward_responsible
    @service_request = ServiceRequest.find(params[:service_request_id]).update_attributes(user_responsible_attendance: params[:user_responsible_attendance])
    
    redirect_to action: :index
  end

  def download_file
    directory = "#{ServiceRequest.find(params[:id]).files_directory}/" + "#{params[:filename]}"
    send_file(directory)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service_request
      @service_request = ServiceRequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_request_params
      params.require(:service_request).permit(:date_request, :time_request, :description_problem, :service_id, :maximum_time_of_attendance, :unit_id, :area_requesting, :subarea_requesting, :user_requesting, :user, :date_response, :time_response, :user_responsible_attendance, :date_scheduled, :time_scheduled, :date_service, :time_service, :time_finalization_attendance, :notes, :status_id, :requested_area, :files_directory, :service_number)
    end
end
