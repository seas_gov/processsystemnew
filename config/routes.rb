Rails.application.routes.draw do
  resources :user_areas
  resources :services
  resources :service_requests do
    collection do
      post :areas
      post :subareas
      match :services_area, via: [:get, :post]
      get :answer_request
      get :schedule_request
      get :forward_responsible
      post :save_forward_responsible
      post :save_schedule_request
      post :save_answer_service
    end
  end
  post 'services_area' => 'service_requests#services_area'
  post 'download_file' => 'service_requests#download_file'

  resources :units
  resources :areas
  get 'dashboard/index'
  # patch "process_managers/update" => "process_managers#update", :as => "process_managers/update"
  resources :process_managers do
    collection do
      post :search_process
  	end
  end
  devise_for :users, controllers: { sessions: 'users/sessions' }
  get 'home/index'
  root to: 'dashboard#index'
end
